#!/bin/bash
# SPDX-FileCopyrightText: 2020 Jason Francis <jason@cycles.network>
# SPDX-License-Identifier: CC0-1.0

set -e

if [ -z "$1" ] || [ -z "$2" ]; then
  echo "Usage: $0 [path to modified libgio-2.0.so] [seconds to run]" > /dev/stderr
  exit 1
fi
cd "$(dirname "$0")"

input=./random.bin
secs=$2

[ -d ./build ] || meson build
ninja -C build > /dev/null

echo "concurrency    control         modified       ratio"
for c in {1..30}; do
  printf "   %2d " $c
  control=$(expr $(./build/gioperf $input $c $secs) / $secs)
  printf "%16d "   $control
  modified=$(expr $(LD_PRELOAD="$1" ./build/gioperf $input $c $secs) / $secs)
  printf "%16d"    $modified
  printf "%12.2f"  $(echo "$modified / $control" | bc -l)
  echo
done

