This is a small tool used to test modifications to GIO file performance. It
repeatedly reads a file for a finite amount of time and compares the results
from your system GIO library to a modified version of GIO that you specify. The
library is loaded with `LD_PRELOAD` and then compared to the system GIO (the
"control" in the experiment). To use it, execute the `run-test.sh` script with
the path to your modified GIO shared library and the number of seconds to run.
Example:

```
$ ./run-tests.sh $HOME/glib/build/gio/libgio-2.0.so 1
tasks          control         modified       ratio
    1             7186             8386        1.17
    2             8610            13953        1.62
    3             8468            15631        1.85
    4             8423            16097        1.91
    5             8768            16868        1.92
    6             8690            17649        2.03
    7             8829            18285        2.07
    8             8843            18450        2.09
    9             8709            18768        2.16
   10             8913            19261        2.16
  ...              ...             ...          ...
```

Legend: (higher number of reads completed is better)

- `concurrency` = number of concurrent tasks running at once
- `control` = system libgio, average reads completed per second
- `modified` = modified libgio, average reads completed per second
- `ratio` = `modified` / `control`

A script is also included to graph the results. (python, matplotlib and numpy
are required)

```
# log to results.txt and show a graph afterwards
$ ./run-tests.sh path_to_libgio-2.0.so 1 | tee results.txt >(./gen-graph.py)
```

