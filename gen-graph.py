#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import sys

dinput = sys.argv[1] if len(sys.argv) > 1 else sys.stdin
d = np.genfromtxt(dinput, skip_header=1, usecols=(1,2))

plt.grid()
plt.xlabel('Concurrent tasks')
plt.ylabel('Reads/sec')
plt.ylim(0, np.max(d) * 1.2)
plt.plot(d[:,0], 'o-', label='GThreadPool')
plt.plot(d[:,1], 'o-', label='io_uring')
plt.legend()
plt.show()
