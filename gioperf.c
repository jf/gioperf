/* SPDX-FileCopyrightText: 2020 Jason Francis <jason@cycles.network>
  *SPDX-License-Identifier: CC0-1.0 */

#include <gio/gio.h>
#include <err.h>
#include <stdatomic.h>

GMainLoop *loop;
static _Atomic(guint64) ops_done = 0;

typedef struct
{
  char buf[4096];
} Buffer;

static
void read_done (GObject *object,
                GAsyncResult *result,
                void *data);

static void
open_done (GObject *object,
           GAsyncResult *result,
           void *data)
{
  g_autoptr(GError) error = NULL;
  GFile *file = G_FILE (object);
  GFileInputStream *stream;
  Buffer *buf = data;

  stream = g_file_read_finish (file, result, &error);

  if (!stream)
    {
      g_warning ("%s: %s", g_file_peek_path (file), error->message);
      return;
    }

  g_object_set_data (G_OBJECT (stream), "buf", buf);

  g_input_stream_read_async (G_INPUT_STREAM (stream),
                             buf->buf,
                             sizeof(buf->buf),
                             G_PRIORITY_DEFAULT,
                             NULL,
                             read_done,
                             file);
}

static void
read_done (GObject *object,
           GAsyncResult *result,
           void *data)
{
  gssize res;
  g_autoptr(GError) error = NULL;
  GFile *file = G_FILE (data);
  GInputStream *stream = G_INPUT_STREAM (object);
  Buffer *buf;

  res = g_input_stream_read_finish (stream, result, &error);

  atomic_fetch_add_explicit (&ops_done, 1, memory_order_acq_rel);

  if (res < 0)
    {
      g_warning ("%s: %s", g_file_peek_path (file), error->message);
      return;
    }
  if (!g_seekable_seek (G_SEEKABLE (stream), 0, G_SEEK_SET, NULL, &error))
    {
      g_warning ("%s: %s", g_file_peek_path (file), error->message);
      return;
    }

  buf = g_object_get_data (G_OBJECT (stream), "buf");
  g_input_stream_read_async (G_INPUT_STREAM (stream),
                             buf->buf,
                             sizeof(buf->buf),
                             G_PRIORITY_DEFAULT,
                             NULL,
                             read_done,
                             file);
}

static gboolean
timeout_done (void *data)
{
  g_main_loop_quit (loop);
  return FALSE;
}

int
main (int argc, char *argv[])
{
  g_autoptr(GArray) buffers = NULL;
  g_autoptr(GFile) file;
  int ops;
  int secs;

  if (argc < 4)
    errx (1, "usage: %s [file] [concurrency] [seconds to run]", argv[0]);

  file = g_file_new_for_path (argv[1]);
  ops = atoi (argv[2]);
  secs = atoi (argv[3]);

  loop = g_main_loop_new (NULL, FALSE);
  buffers = g_array_sized_new (FALSE, TRUE, sizeof(Buffer), ops);
  g_array_set_size (buffers, ops);

  for (int i = 0; i < ops; i++)
    {
      Buffer *buf = &g_array_index (buffers, Buffer, i);
      g_file_read_async (file,
                         G_PRIORITY_DEFAULT,
                         NULL,
                         open_done,
                         buf);
    }

  g_timeout_add_seconds (secs, timeout_done, NULL);
  g_main_loop_run (loop);
  guint64 done = atomic_load_explicit (&ops_done, memory_order_acquire);
  g_print("%lu", done);

  return 0;
}
